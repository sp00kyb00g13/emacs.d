(require 'use-package)

(use-package evil
  :ensure t
  :custom
  (evil-want-keybinding nil)
  (evil-want-integration t))

(use-package evil-collection
  :ensure t
  :init
  (evil-collection-init))

(use-package nlinum-relative
  :ensure t
  :config
  (global-nlinum-relative-mode t)
  (nlinum-relative-setup-evil)
  :custom
  (nlinum-relative-redisplay-delay 0.05)
  )

(use-package neotree
  :ensure t
  :custom
  (neo-show-hidden-files t)
  (neo-theme 'ascii)
  )

(use-package company
  :ensure t
  :custom
  (company-dabbrev-downcase 0) ; ignore case
  (company-idle-delay 0)
  (company-selection-wrap-around t)
  )

(use-package company-anaconda
  :ensure t
  )

(use-package company-lsp
  :ensure t
  )

;(use-package sr-speedbar
;  :ensure t
;  :custom
;  (speedbar-use-images nil)
;  (sr-speedbar-auto-refresh t)
;  (speedbar-show-unknown-files t)
;  )

(use-package rainbow-delimiters
  :ensure t
  )

(use-package helm
  :custom
  (helm-move-to-line-cycle-in-source t) ; circular helm suggestions
  (helm-move-to-line-cycle-in-source nil) ; scroll through sections
  )

;;; Taken from: https://emacs.stackexchange.com/questions/13314/install-pdf-tools-on-emacs-macosx
; install automake for ac local
; 
;;; Install epdfinfo via 'brew install pdf-tools' and then install the
;;; pdf-tools elisp via the use-package below. To upgrade the epdfinfo
;;; server, just do 'brew upgrade pdf-tools' prior to upgrading to newest
;;; pdf-tools package using Emacs package system. If things get messed
;;; up, just do 'brew uninstall pdf-tools', wipe out the elpa
;;; pdf-tools package and reinstall both as at the start.
;(use-package pdf-tools
;  :defer t
;  :ensure t
;  :config
;  (custom-set-variables
;   '(pdf-tools-handle-upgrades nil)) ; Use brew upgrade pdf-tools instead.
;  (setq pdf-info-epdfinfo-program "/usr/local/bin/epdfinfo")
;  )

;(use-package jedi
;  :ensure t
;  :init
;  (add-hook 'python-mode-hook 'jedi:setup)
;  (add-hook 'python-mode-hook 'jedi:ac-setup)
;  )

(use-package anaconda-mode
  :ensure t
  :init
  (add-hook 'python-mode-hook 'anaconda-mode)
  )

(use-package auctex
  :defer t
  :ensure t
  )

(use-package tramp
  :defer t
  :ensure t
  :custom
  (tramp-default-method "ssh")
  )

(use-package idle-highlight-mode
  :ensure t
  :custom
  (idle-highlight-idle-time 2)
  )

(use-package mu4e
  :defer t
  :ensure nil
  :custom
  (mail-user-agent 'mu4e-user-agent)
  (mu4e-maildir "~/mail/vfemail")
  (mu4e-sent-folder "/Sent")
  (mu4e-drafts-folder "/Drafts")
  (mu4e-trash-folder "/Trash")
  )

(use-package notmuch
  :defer t
  :ensure nil
  )

(use-package org-autolist
  :ensure t
  :init
  (add-hook 'org-mode-hook (lambda () (org-autolist-mode)))
  )

(use-package projectile
  :ensure t
  :custom
  (projectile-mode +1)
  )

(use-package helm-projectile
  :ensure t
  :config
  (helm-projectile-on)
  )

(use-package ccls
  :ensure t
  :commands lsp-ccls-enable
  :init
  (add-hook 'c-mode-hook #'+ccls/enable)
  (add-hook 'c++-mode-hook #'+ccls/enable)
  )

(use-package lsp-mode
  :ensure t
  )
