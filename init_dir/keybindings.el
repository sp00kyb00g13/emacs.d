;;; evil-keybindings
(evil-ex-define-cmd "Q" 'evil-quit)
(evil-ex-define-cmd "E" 'evil-edit)
(evil-ex-define-cmd "Wq" 'evil-save-and-close)
(evil-ex-define-cmd "W" 'evil-write)
(evil-ex-define-cmd "Qa" "quitall")
(evil-ex-define-cmd "Wqa" 'evil-save-and-quit)

(define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
(define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)
(define-key evil-window-map "\C-h" 'evil-window-left)

;;; company
;(define-key company-active-map (kbd "TAB") 'company-select-next-or-abort)
;(define-key company-active-map [tab] 'company-select-next-or-abort)
(define-key company-active-map (kbd "TAB") 'company-complete-common)
(define-key company-active-map [tab] 'company-complete-common)
(define-key company-active-map (kbd "C-n") 'company-complete-common-or-cycle)
(define-key company-active-map (kbd "C-p") 'company-select-previous)
;(define-key company-active-map (kbd "BACKTAB") 'company-select-previous-or-abort)
;(define-key company-active-map [backtab] 'company-select-previous-or-abort)

;;; helm
(global-set-key (kbd "C-s") 'helm-occur)
(global-set-key (kbd "M-f") 'helm-find-files)
(define-key helm-map (kbd "C-j") 'helm-next-line) ; navigate minibuffer
(define-key helm-map (kbd "C-k") 'helm-previous-line) ; navigate minibuffer
; fix helm tabs
(define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
(define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z") #'helm-select-action)

;;; helm-gtags
(with-eval-after-load 'helm-gtags
  (define-key helm-gtags-mode-map (kbd "M-t") 'helm-gtags-dwim)
  (define-key helm-gtags-mode-map (kbd "M-T") 'helm-gtags-find-tag)
  (define-key helm-gtags-mode-map (kbd "M-r") 'helm-gtags-find-rtag)
  (define-key helm-gtags-mode-map (kbd "M-s") 'helm-gtags-find-symbol)
  (define-key helm-gtags-mode-map (kbd "M-f") 'helm-gtags-find-files)
  ;(define-key helm-gtags-mode-map [f9] 'helm-gtags-parse-file)
  (define-key helm-gtags-mode-map (kbd "C-k") 'helm-gtags-previous-history)
  (define-key helm-gtags-mode-map (kbd "M->") 'helm-gtags-next-history)
  (define-key helm-gtags-mode-map (kbd "M-<") 'helm-gtags-previous-history)
  )

;;; ibuffer
(global-set-key (kbd "C-x B") 'ibuffer)
(global-set-key (kbd "C-x C-b") 'switch-to-buffer)
;(global-set-key (kbd "C-x b") 'switch-to-buffer)
(global-set-key (kbd "C-x b") 'helm-buffers-list)

;;; neotree
(global-set-key [f8] 'neotree-toggle)
(add-hook 'neotree-mode-hook
	  (lambda ()
	    (define-key evil-normal-state-local-map (kbd "TAB") 'neotree-enter)
	    (define-key evil-normal-state-local-map (kbd "SPC") 'neotree-enter)
	    (define-key evil-normal-state-local-map (kbd "q") 'neotree-hide)
            (define-key evil-normal-state-local-map (kbd "RET") 'neotree-enter)))

;;; pdf-tools
;(require 'pdf-outline)
;(require 'pdf-occur)
;(require 'pdf-annot)
;(require 'pdf-util)
;(define-key pdf-view-mode-map (kbd "G") 'noct:pdf-view-goto-page)
;
;(define-key pdf-view-mode-map (kbd ":") 'evil-ex)
;(define-key pdf-view-mode-map (kbd "C-w l") 'evil-window-right)
;(define-key pdf-view-mode-map (kbd "C-w h") 'evil-window-left)
;(define-key pdf-view-mode-map (kbd "j") 'pdf-view-next-line-or-next-page)
;(define-key pdf-view-mode-map (kbd "k") 'pdf-view-previous-line-or-previous-page)
;(define-key pdf-view-mode-map (kbd "l") 'image-forward-hscroll)
;(define-key pdf-view-mode-map (kbd "h") 'image-backward-hscroll)
;(define-key pdf-view-mode-map (kbd "C-f") 'pdf-view-next-page)
;(define-key pdf-view-mode-map (kbd "C-b") 'pdf-view-previous-page)
;(define-key pdf-view-mode-map (kbd "o") 'pdf-outline )
;(define-key pdf-view-mode-map (kbd "O") 'pdf-outline-quit)
;(define-key pdf-view-mode-map (kbd "+") 'pdf-view-enlarge)
;(define-key pdf-view-mode-map (kbd "-") 'pdf-view-shrink)
;(define-key pdf-view-mode-map (kbd "=") 'pdf-view-scale-reset)
;(define-key pdf-view-mode-map (kbd "W") 'pdf-view-fit-width-to-window)
;(define-key pdf-view-mode-map (kbd "r") 'revert-buffer)
;(define-key pdf-view-mode-map [down-mouse-1] 'pdf-view-mouse-set-region)
;(define-key pdf-view-mode-map (kbd "/") 'isearch-forward)
;(define-key pdf-view-mode-map (kbd "C-s") 'pdf-occur)
;(define-key pdf-annot-minor-mode-map (kbd "at") 'pdf-annot-add-text-annotation)
;
;(evil-set-initial-state 'pdf-outline-buffer-mode 'normal)
;(evil-define-key 'normal pdf-outline-buffer-mode-map
;  (kbd "j") 'next-line
;  (kbd "k") 'previous-line
;  (kbd "h") 'hide-subtree
;  (kbd "l") 'show-subtree
;  (kbd "q") 'quit-window
;  (kbd "RET") 'pdf-outline-follow-link)
;(evil-define-key 'normal pdf-annot-edit-contents-minor-mode-map
;  (kbd "q") 'pdf-annot-edit-contents-commit)
;(evil-define-key 'normal pdf-annot-minor-mode-map
;  (kbd "at") 'pdf-annot-add-text-annotation)
;
;(evil-define-key 'normal pdf-occur-buffer-mode-map
;  (kbd "RET") 'pdf-occur-goto-occurrence
;  (kbd "q") 'quit-window)

;;; python
(define-key anaconda-mode-map (kbd "M-t") 'anaconda-mode-find-definitions)
(define-key anaconda-mode-map (kbd "M-a") 'anaconda-mode-find-assignments)
(define-key anaconda-mode-map (kbd "M-<") 'xref-pop-marker-stack)
(define-key anaconda-mode-map (kbd "M-r") 'anaconda-mode-find-references)
(define-key xref--button-map (kbd "RET") 'xref-quit-and-goto-xref)


;;; speedbar
(global-set-key [f9] 'speedbar-get-focus)
;(global-set-key [f9] 'speedbar-toggle)
;(global-set-key [f10] 'speedbar-select-window)

;;; tex
(define-key evil-insert-state-map (kbd "C-c c") 'insert-coordinates)
(define-key flyspell-mode-map (kbd "C-;") 'flyspell-correct-previous)
(define-key reftex-toc-mode-map (kbd "j") 'reftex-toc-next)
(define-key reftex-toc-mode-map (kbd "k") 'reftex-toc-previous)

;;; misc
(global-set-key [f2] 'sp00ky-idle-highlight-word-at-point)
(define-key ibuffer-mode-map "e" 'ibuffer-ediff-marked-buffers)
(global-set-key (kbd "ESC ESC") 'keyboard-quit)
(define-key key-translation-map (kbd "ESC") (kbd "C-g"))


;;; projectile
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
