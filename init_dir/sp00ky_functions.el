
(defun sp00ky-idle-highlight-word-at-point ()
  "Highlight the word under the point."
  (interactive)
  (let* ((target-symbol (symbol-at-point))
         (target (symbol-name target-symbol)))
    (idle-highlight-unhighlight)
    (when (and target-symbol
               (not (in-string-p))
               (looking-at-p "\\s_\\|\\sw") ;; Symbol characters
               (not (member target idle-highlight-exceptions)))
      (setq idle-highlight-regexp (concat "\\<" (regexp-quote target) "\\>"))
      (highlight-regexp idle-highlight-regexp 'idle-highlight))))
