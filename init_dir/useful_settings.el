;; useful settings/ customizations
;; close the menubars for emacs
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
;; insert matching paren
(electric-pair-mode t)
;(require 'aggressive-indent)
;(global-aggressive-indent-mode 1)


;(kill-buffer "*scratch*") ; kill scratch buffer on start up
(global-hl-line-mode +1) ; highlight current line
(column-number-mode t) ; show column number on bottom of screen

(setq inhibit-startup-screen t)
(setq make-backup-files nil)
(setq backup-inhibited t)
(setq auto-save-default nil)

; doesnt work atm
(setq x-select-enable-clipboard t) ; copy to system clipboard

;split vertically by default when opening two files
(setq split-height-threshold nil)
(setq split-width-threshold 0)

;; Term-mode remap prefix key
(add-hook 'term-mode-hook
          '(lambda ()
          (term-set-escape-char ?\C-x)))

;;; changing title of duplicate buffer titles
(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse)

;fix emacs comments spacing
(setq ess-fancy-comments nil)

;;disable ediff white space
(setq ediff-diff-options "-w")

;; ediff marked buffers in ibuffer mode
(defun ibuffer-ediff-marked-buffers ()
  (interactive)
  (let* ((marked-buffers (ibuffer-get-marked-buffers))
         (len (length marked-buffers)))
    (unless (= 2 len)
      (error (format "%s buffer%s been marked (needs to be 2)"
                     len (if (= len 1) " has" "s have"))))
    (ediff-buffers (car marked-buffers) (cadr marked-buffers))))
(require 'ibuffer)

(setq large-file-warning-threshold nil) ; surpress large file warning

;;;use this to make the gui version less annoying

;(global-set-key [f3] 'highlight-symbol)
;(global-set-key [(control f3)] 'highlight-symbol-next)
;(global-set-key [(shift f3)] 'highlight-symbol-prev)

;(global-semantic-decoration-mode 1)
;(semanticdb-enable-gnu-global-databases 'c-mode)
;(global-ede-mode t)

;; syntax checker flycheck
;(add-hook 'after-init-hook #'global-flycheck-mode)
; error showing up console
;(with-eval-after-load 'flycheck
;  (flycheck-pos-tip-mode))
;;;;(setq flycheck-gcc-include-path '("/home/gi/multimedia/code/combox/src/back_end/include"))
;(setq flycheck-disabled-checkers '(c/c++-clang))
;(setq flycheck-enabled-checkers '(c/c++-gcc))
;(add-to-list 'default-frame-alist
;	     '(font . "Consolas-14"))

(add-to-list 'default-frame-alist
	     '(font . "DejaVuSansMono-16"))
(desktop-save-mode 1)
;(setq desktop-restore-frames nil)
;(global-font-lock-mode 0)
;(setq jit-lock-defer-time 0)

;(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
;(setq highlight-indent-guides-method 'character)
;(setq highlight-indent-guides-auto-character-face-perc 30)
