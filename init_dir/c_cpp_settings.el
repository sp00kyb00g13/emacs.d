;; tab settings
;(add-hook 'c++-mode-hook 'irony-mode)
;(add-hook 'c-mode-hook 'irony-mode)
;(add-hook 'objc-mode-hook 'irony-mode)
;(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(setq ccls-executable "/usr/bin/ccls")

(add-to-list 'auto-mode-alist '("\\.cfs\\'" . asm-mode))
(add-to-list 'auto-mode-alist '("\\.fs\\'" . asm-mode))
(add-to-list 'auto-mode-alist '("\\.cfi\\'" . asm-mode))
(add-to-list 'auto-mode-alist '("\\.hct\\'" . asm-mode))

(setq c-default-style "linux")
(setq-default tab-width 8)
(setq-default c-basic-offset 8)
(setq-default c-indent-level 8)
(setq-default c-brace-imaginary-offset 0)
(setq-default c-brace-offset -8)
(setq-default c-argdecl-indent 8)
(setq-default c-label-offset -8)
(setq-default c-continued-statement-offset 8)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 8)
(setq-default global-semantic-decoration-mode 1) ;highlight header files that cant be found
(setq-default fill-column 80)

;(add-hook 'c-mode-common-hook
;          (lambda ()
;            (when (derived-mode-p 'c-mode 'c++-mode)
;              (ggtags-mode 1)
;              (idle-highlight-mode 1))))

; from enberg on #emacs
(setq compilation-finish-function
  (lambda (buf str)
    (if (null (string-match ".*exited abnormally.*" str))
        ;;no errors, make the compilation window go away in a few seconds
        (progn
          (run-at-time
           "2 sec" nil 'delete-windows-on
           (get-buffer-create "*compilation*"))
          (message "No Compilation Errors!")))))
