(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
;(add-to-list 'load-path "~/.emacs.d/submodules/evil/")
;(add-to-list 'load-path "~/.emacs.d/submodules/aggressive-indent-mode/")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(package-initialize)

(load-theme 'sp00ky t)
;(load-theme 'monokai t)

(load "~/.emacs.d/init_dir/use_package.el")
(load "~/.emacs.d/init_dir/evil.el")
(load "~/.emacs.d/init_dir/c_cpp_settings.el")
(load "~/.emacs.d/init_dir/tex.el")
(load "~/.emacs.d/init_dir/useful_settings.el")
;(load "~/.emacs.d/init_dir/evil-evilified-state.el")
;(load "~/.emacs.d/init_dir/notmuch.el")
;(load "~/.emacs.d/init_dir/pdf_tools.el")
(load "~/.emacs.d/init_dir/ibuffer.el")
(load "~/.emacs.d/init_dir/completion.el")
(load "~/.emacs.d/init_dir/sp00ky_functions.el")
(load "~/.emacs.d/init_dir/keybindings.el")
(load "~/.emacs.d/init_dir/python.el")

;(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;; scroll one line at a time
; Autosave every 500 typed characters
(setq auto-save-interval 500)
; Scroll just one line when hitting bottom of window
(setq scroll-conservatively 10000)


;; paren mode, highlight matching parens
(show-paren-mode 1)

;; evil plugin
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("0e01d78ec977390f023600a356e862e94a85a3b1673099b7468654135633e69d" "31c1aee3d4b62b9f8476359853886ecf6966685c5420f4d6f16daa80fba5e8d0" "f78de13274781fbb6b01afd43327a4535438ebaeec91d93ebdbba1e3fba34d3c" default)))
 '(helm-gtags-ignore-case t t)
 '(helm-gtags-path-style (quote relative))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (org-autolist pythonic use-package company-c-headers function-args helm-gtags helm helm-ebdb company-irony-c-headers neotree company-irony irony evil-collection pdf-tools flyspell-correct-popup auctex ggtags grandshell-theme alect-themes cyberpunk-theme counsel-gtags counsel highlight-indent-guides smooth-scroll smooth-scrolling nlinum-relative flycheck sr-speedbar company all-the-icons monokai-theme)))
 '(speedbar-show-unknown-files t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; ediff
(setq ediff-split-window-function 'split-window-horizontally)
;(setq-default ediff-forward-word-function 'forward-char)

;; reverting buffer
(global-auto-revert-mode t)
(global-set-key [f5] 'revert-all-buffers)
(defun revert-all-buffers ()
  "Refreshes all open buffers from their respective files."
  (interactive)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (and (buffer-file-name) (file-exists-p (buffer-file-name)) (not (buffer-modified-p)))
        (revert-buffer t t t) )))
  (message "Refreshed open files.") )
                                        ; indentation markers

(setq help-window-select t)
